from pandas_datareader import data
import pandas as pd
def create_df():
    stock = ['AMZN']

    # We would like all available data from Feb 2020 to Mar 2020
    start_date = '2020-02-01'
    end_date = '2020-03-31'
    Amzn_data = data.DataReader(stock, 'yahoo', start_date, end_date)
    return Amzn_data