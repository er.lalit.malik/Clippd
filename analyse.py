import pandas as pd
import numpy as np
from check_read import create_df
def analyze(): 
    trade_data = pd.read_csv("traders_data.csv")
    trade_refined_data = trade_data[(trade_data.stockSymbol == "AMZN") & (trade_data.tradeDatetime >= "2020-02-01 00:00:00")& (trade_data.tradeDatetime <= "2020-03-31 00:00:00")]
    trade_refined_data['tradeDatetime'] = pd.to_datetime(trade_refined_data['tradeDatetime'])
    trade_refined_data['tradeDatetime'] = trade_refined_data['tradeDatetime'].dt.date
    trade_refined_data = trade_refined_data.rename(columns = {'tradeDatetime':'Date'})
    amzn_data = create_df()
    trade_refined_data['Date']= pd.to_datetime(trade_refined_data['Date'])
    joined =  pd.merge(trade_refined_data,amzn_data,on='Date', how= 'inner')
    ###### Combined data frame is ready, now we can compare the prices#################
    return joined
    
def compare(suspected):
    suspected = suspected() # now call analyze inside compare
    suspected.rename({"(High, AMZN)": "high"}, axis=1,inplace = True)
    namesList = ['adclose','close','high','low','open','volume']
    suspected.columns = suspected.columns[:10].tolist() + namesList
    suspected['suspected'] = np.where((suspected['price']> suspected['high']) | (suspected['price'] <= suspected['low']) ,'yes', 'no')

    
def order_placed_not_traded(traded):  #taking joined datframe because its result of inner, subtracting would give desired result
    amzn_data = create_df()
    traded = traded()
    #print(traded.columns)
    df = pd.DataFrame(columns=['Date'])
    df['Date']= pd.to_datetime(df['Date'])

    joined2 =  pd.merge(amzn_data,df, on ='Date',left_on=None ,how ='left')
    joined2.to_csv('joined2.csv')
    traded.to_csv('traded11.csv')
    notraded = traded[(~traded['Date'].isin(joined2['Date']))] #this will give records where trading is done on non trading day 
    print(notraded)
     
if __name__ == "__main__":
    compare(analyze)
    order_placed_not_traded(analyze)